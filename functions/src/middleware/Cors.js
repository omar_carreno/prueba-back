"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Cors {
    static apply(request, response, next) {
        response.header("Access-Control-Allow-Origin", "*");
        response.header("Access-Control-Allow-Methods", "GET,PUT,PATCH,POST,DELETE");
        response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, token, X-AUTH-TOKEN");
        next();
    }
}
exports.Cors = Cors;
