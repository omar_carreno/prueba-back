"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("../config");
class Auth {
    static apply(request, response, next) {
        const authorization = request.headers.authorization || "";
        config_1.database.Database.reference.auth()
            .verifyIdToken(authorization)
            .then((tokenInfo) => {
            request.authUser = tokenInfo.uid;
            next();
        })
            .catch(() => {
            response.status(400).send({
                message: "Bad request auth",
            });
        });
    }
}
exports.Auth = Auth;
