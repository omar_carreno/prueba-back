"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const inversify_1 = require("inversify");
require("reflect-metadata");
const XLSX = require("xlsx");
const ImportExcelDta_1 = require("./ImportExcelDta");
let ImpotExcelBns = class ImpotExcelBns {
    constructor() {
        this.importExcelDta = new ImportExcelDta_1.ImportExcelDta();
    }
    impotExcel(data64, response) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                console.log('🤬 data64', data64);
                const workbook = XLSX.read(data64, { type: "base64", WTF: false });
                const worksheet = workbook.Sheets[workbook.SheetNames[0]];
                const list = XLSX.utils.sheet_to_json(worksheet, { header: 1 });
                if (list.length === 0) {
                    throw ({
                        code: "NODATA",
                        status: false,
                    });
                }
                const users = {};
                for (let row = 1; row < list.length; row++) {
                    if (list[row][0]) {
                        const id = list[row][0];
                        users[id] = {};
                        for (let column = 0; column < list[row].length; column++) {
                            if (list[row][column] && list[0][column]) {
                                const element = list[row][column];
                                const title = list[0][column];
                                users[id][title] = element;
                            }
                        }
                    }
                }
                yield this.importExcelDta.setDescriptionPayuToRechage(users);
                return response.status(200).send(data64);
            }
            catch (error) {
                return response.status(500).send(error);
            }
        });
    }
};
ImpotExcelBns = __decorate([
    inversify_1.injectable(),
    __metadata("design:paramtypes", [])
], ImpotExcelBns);
exports.ImpotExcelBns = ImpotExcelBns;
