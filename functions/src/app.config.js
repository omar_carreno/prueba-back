"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require("firebase-admin");
const database = require("./config/data-instance.json");
exports.database = database;
const PROYECT_ID = database.project_id;
exports.PROYECT_ID = PROYECT_ID;
let adminData;
exports.adminData = adminData;
exports.adminData = adminData = admin.initializeApp({
    credential: admin.credential.cert(database),
    databaseURL: "https://test-ab458.firebaseio.com",
});
const TYPES_INJECT = {
    IMPORTEXCEL: Symbol("importExcel"),
};
exports.TYPES_INJECT = TYPES_INJECT;
const VERSION = "v1";
exports.VERSION = VERSION;
