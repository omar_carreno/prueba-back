"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const inversify_1 = require("inversify");
const app_config_1 = require("./app.config");
const ImportExcelBns_1 = require("./lib/ImportExcelBns");
const container = new inversify_1.Container();
exports.container = container;
container.bind(app_config_1.TYPES_INJECT.IMPORTEXCEL)
    .to(ImportExcelBns_1.ImpotExcelBns);
