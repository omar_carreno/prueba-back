"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Auth_1 = require("../../src/middleware/Auth");
const Cors_1 = require("../../src/middleware/Cors");
const app_config_1 = require("../app.config");
const inversify_config_1 = require("../inversify.config");
const express = require("express");
class ImportExcelPt {
    constructor() {
        this.importExcelPt = express();
        this.importExcelPt.use(Cors_1.Cors.apply);
        const routerimportExcel = express.Router();
        routerimportExcel.route("/imporExcel")
            .post(Auth_1.Auth.apply, (request, response) => {
            const impotExcelBns = inversify_config_1.container.get(app_config_1.TYPES_INJECT.IMPORTEXCEL);
            impotExcelBns.impotExcel(request.body.file, response);
        });
        this.importExcelPt.use(routerimportExcel);
    }
}
exports.ImportExcelPt = ImportExcelPt;
