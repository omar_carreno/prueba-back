"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bodyParser = require("body-parser");
const express = require("express");
const ImportExcelPt_1 = require("./src/services/ImportExcelPt");
const app = express();
exports.app = app;
app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
app.use(bodyParser.text());
const importExcelInstance = new ImportExcelPt_1.ImportExcelPt();
app.use("/importExcel", importExcelInstance.importExcelPt);
const PORT = process.env.PORT || 9090;
app.listen(PORT, () => {
    process.stdout.write(`App listening on port ${PORT}\n`);
    process.stdout.write("Press Ctrl+C to quit.\n");
});
