import { Response } from "express";
import { Auth } from "../../src/middleware/Auth";
import { Cors } from "../../src/middleware/Cors";
import { TYPES_INJECT } from "../app.config";
import { container } from "../inversify.config";
import { ImpotExcelBns } from "../lib/ImportExcelBns";
import { IRequest } from "../lib/util/IRequest";

import * as express from "express";

export class ImportExcelPt {
    public importExcelPt: express.Express;
    constructor() {
        this.importExcelPt = express();
        this.importExcelPt.use(Cors.apply);
        const routerimportExcel = express.Router();
        routerimportExcel.route("/imporExcel")
            .post(
                Auth.apply,
                (request: IRequest, response: Response) => {
                    const impotExcelBns = container.get<ImpotExcelBns>(TYPES_INJECT.IMPORTEXCEL);
                    impotExcelBns.impotExcel(request.body.file, response);
                });
        this.importExcelPt.use(routerimportExcel);
    }
}
