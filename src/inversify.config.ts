import { Container } from "inversify";
import { TYPES_INJECT } from "./app.config";
import { ImpotExcelBns } from "./lib/ImportExcelBns";

const container = new Container();

container.bind<ImpotExcelBns>(TYPES_INJECT.IMPORTEXCEL)
    .to(ImpotExcelBns);
export { container };
