import * as admin from "firebase-admin";
import { adminData } from "../../app.config";

export class Database {
    public static reference: admin.app.App = adminData;
}
