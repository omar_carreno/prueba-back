import { database } from "../config";

export class ImportExcelDta {

    public setDescriptionPayuToRechage(dataStorage: any): Promise<any> {
        return new Promise((resolve, reject) => {
            database.Database.reference.database().ref()
                .child("/users")
                .update(dataStorage)
                .then(() => resolve())
                .catch((error: any) => {
                    reject({
                        error,
                        lugar: this.constructor.name,
                    });
                });
        });
    }

}
