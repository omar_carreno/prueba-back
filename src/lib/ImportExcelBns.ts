import { Response } from "express";
import { injectable } from "inversify";
import "reflect-metadata";
import * as XLSX from "xlsx";
import { ImportExcelDta } from "./ImportExcelDta";

@injectable()

class ImpotExcelBns {

  private importExcelDta: ImportExcelDta;

  constructor() {
    this.importExcelDta = new ImportExcelDta();
  }

  public async impotExcel(data64: any, response: Response): Promise<any> {
    try {
      const workbook = XLSX.read(data64, { type: "base64", WTF: false });
      const worksheet = workbook.Sheets[workbook.SheetNames[0]];
      const list: any = XLSX.utils.sheet_to_json(worksheet, { header: 1 });
      if (list.length === 0) {
        throw ({
          code: "NODATA", // El archivo  no contiene datos
          status: false,
        });
      }
      const users: any = {};
      for (let row = 1; row < list.length; row++) {
        if (list[row][0]) {
          const id = list[row][0];
          users[id] = {};
          for (let column = 0; column < list[row].length; column++) {
            if (list[row][column] && list[0][column]) {
              const element = list[row][column];
              const title = list[0][column];
              users[id][title] = element;
            }
          }
        }
      }
      await this.importExcelDta.setDescriptionPayuToRechage(users);
      return response.status(200).send(data64);
    } catch (error) {
      return response.status(500).send(error);
    }
  }

}
export { ImpotExcelBns };
