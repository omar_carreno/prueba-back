import { Request } from "express";

export interface IRequest extends Request {
    authUser: string;
    entity: any;
    roleUser: any;
    metaData: any;
    check<T>(entity: T): Promise<any>;
}
