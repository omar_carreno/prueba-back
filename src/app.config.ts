import * as admin from "firebase-admin";

const database = require("./config/data-instance.json");
const PROYECT_ID: string = database.project_id;
let adminData: admin.app.App;

adminData = admin.initializeApp({
    credential: admin.credential.cert(database),
    databaseURL: "https://test-ab458.firebaseio.com",
});
const TYPES_INJECT = {
    IMPORTEXCEL: Symbol("importExcel"),
};

const VERSION = "v1";

export {
    database,
    adminData,
    VERSION,
    TYPES_INJECT,
    PROYECT_ID,
};
