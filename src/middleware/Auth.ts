import { NextFunction, Response } from "express";
import { database } from "../config";
import { IRequest } from "../lib/util/IRequest";
export class Auth {

    public static apply(request: IRequest, response: Response, next: NextFunction) {
        const authorization: string = request.headers.authorization || "";
        database.Database.reference.auth()
            .verifyIdToken(authorization)
            .then((tokenInfo: { uid: any; }) => {
                request.authUser = tokenInfo.uid;
                next();
            })
            .catch(() => {
                response.status(400).send({
                    message: "Bad request auth",
                });
            });
    }
}
