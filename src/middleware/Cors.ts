import { NextFunction, Request, Response } from "express";

export class Cors {
    public static apply(request: Request, response: Response, next: NextFunction) {
      response.header("Access-Control-Allow-Origin", "*");
      response.header("Access-Control-Allow-Methods", "GET,PUT,PATCH,POST,DELETE");
      // tslint:disable-next-line:max-line-length
      response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, token, X-AUTH-TOKEN");
      next();
    }
}
