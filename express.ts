import * as bodyParser from "body-parser";
import * as express from "express";
import {
    ImportExcelPt,
} from "./src/services/ImportExcelPt";

const app = express();
app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));
app.use(bodyParser.text());

const importExcelInstance = new ImportExcelPt();

app.use("/importExcel", importExcelInstance.importExcelPt);

const PORT = process.env.PORT || 9090;
app.listen(PORT, () => {
  process.stdout.write(`App listening on port ${PORT}\n`);
  process.stdout.write("Press Ctrl+C to quit.\n");
});

export { app };
